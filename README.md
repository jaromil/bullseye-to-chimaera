# Run with Ansible

Tested on a vanilla Debian 11 remotely (comes back with ssh open)


```
---
- name: INSTALL DEVUAN
  hosts: all
#  become: yes
  tasks:
    - name: Install Devuan keyring
      shell: >-
        curl https://pkgmaster.devuan.org/devuan/pool/main/d/devuan-keyring/devuan-keyring_2017.10.03_all.deb -o devuankeys.deb && dpkg -i devuankeys.deb

    - name: Install sysvinit
      apt:
        force: yes
        name: sysvinit-core
        update_cache: yes

    - name: Disable DBus
      command: update-rc.d dbus disable

    - name: Clean up packages
      apt:
        force: yes
        autoremove: yes
        purge: yes

    - name: Reboot server
      shell: init 6
      async: 1
      poll: 0
      ignore_errors: true

    - name: Waiting for server to come back
      local_action:
        module: wait_for
        host: "{{ inventory_hostname }}"
        state: started
        delay: 30
        timeout: 60

    - name: Remove current apt source list
      replace:
        backup: true
        dest: /etc/apt/sources.list
        regexp: '^deb (?!http://deb.devuan.org).*'

    - name: Add Devuan apt source
      lineinfile:
        dest: /etc/apt/sources.list
        line: 'deb http://deb.devuan.org/merged chimaera main'
        state: present

    - name: Migrate to Devuan (dist-upgrade, takes time...)
      apt:
        upgrade: dist
        update_cache: yes

    - name: Remove and purge systemd
      apt:
        state: absent
        name: dbus-user-session libnss-systemd systemd plymouth
        purge: yes
        autoremove: yes

    - name: Reboot server
      shell: init 6
      async: 1
      poll: 0
      ignore_errors: true

    - name: Waiting for server to come back
      local_action:
        module: wait_for
        host: "{{ inventory_hostname }}"
        state: started
        delay: 30
        timeout: 60
```
